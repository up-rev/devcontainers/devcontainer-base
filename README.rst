Base Docker 
===========

This is the base docker image for uprev devcontainers. It has all of the basic tools used for development and documentation. This image should be used as a base for new development containers to speed up build times. 

There is a devcontainers.json file, so you can add this repo as a submodule to your project to use as a devcontainer:

.. code::bash 

    git submodule add https://gitlab.com/up-rev-public/devcontainers/base.git .devcontainer


Build Args 
----------

Variables are used in the `FROM` directive so that multiple tags can be set up in CI/CD

- BASE_IMAGE: base image to build from
- BASE_TAG: Tag of base image to use 

.. code:: bash 

    docker build . --build-arg "BASE_IMAGE=ubuntu" --build-arg "BASE_TAG=22.04" . -t uprev/base:ubuntu-22.04