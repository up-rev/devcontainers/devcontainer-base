ARG BASE_IMAGE=ubuntu
ARG BASE_TAG=22.04
FROM ${BASE_IMAGE}:${BASE_TAG} as dev


ENV DEBIAN_FRONTEND noninteractive


#passwords as arguments so they can be changed
ARG DEV_PW=password

RUN apt-get update && apt-get install -y --fix-missing --no-install-recommends \ 
    wget \ 
    cmake \
    curl \
    build-essential \
    openssl \
    make \ 
    git \
    zlib1g-dev \
    python3 \
    python3-pip \
    python3-setuptools \
    plantuml \
    graphviz \
    texlive \
    latexmk \
    texlive-science \
    texlive-formats-extra \ 
    tex-gyre && \
    apt-get autoremove -y && \
    rm -rf /var/lib/apt/lists/* 

# #Copy the latest release of plantuml. Ubuntu 18.04 package manager has an older one without timing diagram support
# RUN wget https://github.com/plantuml/plantuml/releases/download/v1.2022.1/plantuml-1.2022.1.jar -O /usr/share/plantuml/plantuml.jar

RUN python3 -m pip install -U --force-reinstall pip


#sphinx dependencies 
RUN pip3 install wheel sphinx sphinxcontrib-plantuml sphinx-rtd-theme recommonmark restview docxbuilder docxbuilder[math]  sphinx-rtd-dark-mode pyyaml


#mrtutils 
RUN pip3 install mrtutils


# Add user dev to the image
RUN adduser --quiet dev && \
    echo "dev:$DEV_PW" | chpasswd && \
    chown -R dev /home/dev 

